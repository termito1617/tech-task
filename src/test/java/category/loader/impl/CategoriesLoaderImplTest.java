package category.loader.impl;

import category.entities.AnimalsCategory;
import category.entities.Category;
import category.entities.NumbersCategory;
import category.loader.CategoriesLoader;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.ByteArrayInputStream;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

@RunWith(JUnit4.class)
public class CategoriesLoaderImplTest {

    private List<String> animalItems = Arrays.asList("a1", "a2", "a3", "A3");
    private List<String> numberItems = Arrays.asList("n1", "n2", "n3");

    @Test
    public void shouldLoadCategories() {
        String input = new InputBuilder()
                .add("ANIMALS", animalItems)
                .add("NUMBERS", numberItems)
                .build();
        CategoriesLoader loader = new CategoriesLoaderImpl(new ByteArrayInputStream(input.getBytes()));

        List<Category> categories = Arrays.asList(new AnimalsCategory(), new NumbersCategory());
        loader.loadCategoriesData(categories);

        Assert.assertTrue(animalItems.containsAll(categories.get(0).getItems()));
        Assert.assertTrue(numberItems.containsAll(categories.get(1).getItems()));
    }

    @Test
    public void shouldLoadAllCategoryItemsWhenTheCategoryAppearsSeveralTimes() {
        String input = new InputBuilder()
                .add("ANIMALS", animalItems.subList(0, 2))
                .add("NUMBERS", numberItems)
                .add("ANIMALS", animalItems.subList(2, 3))
                .add("NUMBERS", Collections.emptySet())
                .add("ANIMALS", animalItems.subList(3, 4))
                .build();
        CategoriesLoader loader = new CategoriesLoaderImpl(new ByteArrayInputStream(input.getBytes()));

        List<Category> categories = Arrays.asList(new AnimalsCategory(), new NumbersCategory());
        loader.loadCategoriesData(categories);

        Assert.assertTrue(animalItems.containsAll(categories.get(0).getItems()));
        Assert.assertTrue(numberItems.containsAll(categories.get(1).getItems()));
    }

    private class InputBuilder {
        private StringBuilder stringBuilder = new StringBuilder();
        private String ls = System.lineSeparator();

        InputBuilder add(String name, Collection<String> items) {
            stringBuilder.append(name);
            stringBuilder.append(ls);
            for(String item : items) {
                stringBuilder.append(item);
                stringBuilder.append(ls);
            }
            return this;
        }

        String build() {
            return stringBuilder.toString();
        }
    }
}
