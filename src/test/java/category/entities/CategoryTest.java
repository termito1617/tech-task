package category.entities;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.Set;

@RunWith(JUnit4.class)
public class CategoryTest {
    private static final String CATEGORY_NAME = "test";

    @Test
    public void shouldReturnCategoryName() {
        Category category = new Category(CATEGORY_NAME) {};

        Assert.assertEquals(CATEGORY_NAME, category.getName());
    }

    @Test
    public void shouldReturnItems() {
        Category category = new Category(CATEGORY_NAME) {};
        category.addItem("item1");
        category.addItem("item2");

        Set<String> items = category.getItems();

        Assert.assertEquals(2, items.size());
        Assert.assertTrue(items.contains("item1"));
        Assert.assertTrue(items.contains("item2"));
    }

    @Test(expected = UnsupportedOperationException.class)
    public void shouldReturnUnmodifiableSetOfItems() {
        Category category = new Category(CATEGORY_NAME) {};
        category.addItem("item1");

        category.getItems().add("test");
    }

    @Test
    public void shouldConvertToString() {
        Category category = new Category(CATEGORY_NAME) {};
        category.addItem("item1");
        category.addItem("item2");

        Assert.assertEquals(category.getName() + ":\nitem1\nitem2", category.toString());
    }
}
