package category.entities;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class NumbersCategoryTest {

    @Test
    public void shouldReturnCorrectName() {
        Category category = new NumbersCategory();
        Assert.assertEquals("NUMBERS", category.getName());
    }
}
