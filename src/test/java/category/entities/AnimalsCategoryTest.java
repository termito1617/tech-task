package category.entities;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class AnimalsCategoryTest {
    @Test
    public void shouldReturnCorrectName() {
        Category category = new AnimalsCategory();
        Assert.assertEquals("ANIMALS", category.getName());
    }
}
