package category.entities;

import org.apache.commons.codec.digest.DigestUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.Set;

@RunWith(JUnit4.class)
public class CarsCategoryTest {

    @Test
    public void shouldReturnCorrectName() {
        Category category = new CarsCategory();
        Assert.assertEquals("CARS", category.getName());
    }

    @Test
    public void shouldStoreItemsCaseSensitive() {
        Category category = new CarsCategory();
        category.addItem("car");
        category.addItem("Car");
        category.addItem("CAR");

        Set<String> items = category.getItems();

        Assert.assertEquals(1, items.size());
        Assert.assertTrue(items.contains("car"));
    }
    @Test
    public void shouldConvertToStringWithReversOrderAndHash() {
        Category category = new CarsCategory();
        category.addItem("a");
        category.addItem("b");

        Assert.assertEquals(category.getName() + ":\n" +
                "b (" + DigestUtils.md5Hex("b") + ")\n" +
                "a (" + DigestUtils.md5Hex("a") + ")", category.toString());
    }
}
