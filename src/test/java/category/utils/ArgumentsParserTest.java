package category.utils;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.Map;

@RunWith(JUnit4.class)
public class ArgumentsParserTest {
    @Test
    public void shouldReturnEmptyMapWhenThereIsNoArguments() {
        Assert.assertTrue(ArgumentsParser.parse(new String[] {}).isEmpty());
    }

    @Test
    public void shouldParseParameter() {
        String key = "key";
        String value = "value";

        String actualValue = ArgumentsParser.parse(new String[] {key + "=" + value}).get(key);

        Assert.assertEquals(value, actualValue);
    }

    @Test
    public void shouldParseSeveralParameters() {
        String[] args = new String[] { "key1=value1", "key2=value2", "key3=value3"};

        Map<String, String> parameters = ArgumentsParser.parse(args);

        Assert.assertEquals(3, parameters.size());
        Assert.assertEquals("value1", parameters.get("key1"));
        Assert.assertEquals("value2", parameters.get("key2"));
        Assert.assertEquals("value3", parameters.get("key3"));
    }
}
