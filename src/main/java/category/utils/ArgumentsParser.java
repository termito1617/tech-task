package category.utils;

import java.util.AbstractMap;
import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Utility class for parsing program arguments.
 **/
public class ArgumentsParser {
    private static final String ARG_KEY_VALUE_DELIMITER = "=";

    /**
     * Parse arguments from sting array.
     *
     * @param args - array of arguments in following format: {"key1=value1", "key2=value2", ...}
     *
     * @return parameters map
     **/
    public static Map<String, String> parse(String[] args) {
        return Arrays.stream(args)
                .map(arg -> {
                    String[] split = arg.split(ARG_KEY_VALUE_DELIMITER);
                    return new AbstractMap.SimpleEntry<>(split[0], split[1]);
                }).collect(Collectors.toMap(AbstractMap.SimpleEntry::getKey, AbstractMap.SimpleEntry::getValue));
    }
}
