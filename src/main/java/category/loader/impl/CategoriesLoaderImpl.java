package category.loader.impl;

import category.entities.Category;
import category.loader.CategoriesLoader;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Categories loader implementation that use input stream as a source of categories`s items.
 * Expected input stream data format is a stream of lines where each line is ether a category`s item or a category
 * itself.
 **/
public class CategoriesLoaderImpl implements CategoriesLoader {

    private BufferedReader reader;
    private Category currentCategory;

    /**
     * Construct a category loader.
     *
     * @param inputStream - categories`s items input stream.
     **/
    public CategoriesLoaderImpl(InputStream inputStream) {
        this.reader = new BufferedReader(new InputStreamReader(inputStream));
    }

    @Override
    public void loadCategoriesData(List<Category> categories) {
        Map<String, Category> nameToCategoryMap = categories.stream()
                .collect(Collectors.toMap(cat -> cat.getName().toUpperCase(), Function.identity()));

        reader.lines().forEachOrdered(line ->
                currentCategory = Optional.ofNullable(nameToCategoryMap.get(line.toUpperCase()))
                    .orElseGet(() -> {
                        currentCategory.addItem(line);
                        return currentCategory;
                    }));
    }

    @Override
    public void close() throws Exception {
        reader.close();
    }
}

