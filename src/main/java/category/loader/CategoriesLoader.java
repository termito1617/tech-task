package category.loader;

import category.entities.Category;

import java.util.List;

public interface CategoriesLoader extends AutoCloseable {

    /**
     * Initialize list of categories.
     *
     * @param categories - list of categories to be initialized.
     **/
    void loadCategoriesData(List<Category> categories);
}
