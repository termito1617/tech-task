package category.entities;

/**
 * The class represent the category of animals.
 **/
public class AnimalsCategory extends Category {
    private static final String ANIMALS_CATEGORY_NAME = "ANIMALS";

    /**
     * Construct an animal category.
     **/
    public AnimalsCategory() {
        super(ANIMALS_CATEGORY_NAME);
    }
}
