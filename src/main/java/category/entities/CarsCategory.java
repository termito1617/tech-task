package category.entities;

import org.apache.commons.codec.digest.DigestUtils;

import java.util.Comparator;
import java.util.stream.Collectors;

/**
 * The class represent the category of cars.
 **/
public class CarsCategory extends Category {
    private static final String CARS_CATEGORY_NAME = "CARS";

    private static final String ITEMS_DELIMITER = "\n";
    private static final String CATEGORY_OUTPUT_FORMAT = "%s:\n%s";
    private static final String HASH_FORMAT = " (%s)";

    /**
     * Construct a car category.
     **/
    public CarsCategory() {
        super(CARS_CATEGORY_NAME);
    }

    /**
     * Add item to the category. Items of the category are case insensitive so they all are converted to lower case.
     *
     * @param item - item name
     **/
    @Override
    public void addItem(String item) {
        super.addItem(item.toLowerCase());
    }

    /**
     * Converts a car category to a string in the following format:
     * <pre>
     *  CategoryName:
     *  item1 (HASH)
     *  item2 (HASH)
     *  ...
     *  itemN (HASH)
     * </pre>
     * Where HASH is a md5 hash of corresponding item.
     * Items are sorted in inverse natural order.
     *
     * @return string representation of the category
     **/
    @Override
    public String toString() {
        String itemsStr = getItems()
                .stream()
                .sorted(Comparator.<String>naturalOrder().reversed())
                .map(item -> item + String.format(HASH_FORMAT, DigestUtils.md5Hex(item)))
                .collect(Collectors.joining(ITEMS_DELIMITER));
        return String.format(CATEGORY_OUTPUT_FORMAT, getName(), itemsStr);
    }
}
