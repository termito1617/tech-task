package category.entities;

/**
 * The class represent the category of numbers.
 **/
public class NumbersCategory extends Category {
    private static final String NUMBERS_CATEGORY_NAME = "NUMBERS";

    /**
     * Construct a number category.
     **/
    public NumbersCategory() {
        super(NUMBERS_CATEGORY_NAME);
    }
}
