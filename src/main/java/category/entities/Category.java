package category.entities;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * The class represent abstract Category entity.
 **/
public abstract class Category {
    private static final String ITEMS_DELIMITER = "\n";
    private static final String CATEGORY_OUTPUT_FORMAT = "%s:\n%s";

    private String name;
    private Set<String> items = new HashSet<>();

    /**
     * Construct a category.
     *
     * @param name - name of category
     **/
    public Category(String name) {
        this.name = name;
    }

    /**
     * Returns name of category.
     *
     * @return category name
     **/
    public String getName() {
        return name;
    }

    /**
     * Adds item to category.
     *
     * @param item - item name
     **/
    public void addItem(String item) {
        items.add(item);
    }

    /**
     * Allows to get unmodifiable set of category`s items.
     *
     * @return set of category`s items
     **/
    public Set<String> getItems() {
        return Collections.unmodifiableSet(items);
    }

    /**
     * Converts the category to a string in the following format:
     * <pre>
     *  CategoryName:
     *  item1
     *  item2
     *  ...
     *  itemN
     * </pre>
     * Items are sorted in natural order.
     *
     * @return string representation of the category
     **/
    @Override
    public String toString() {
        String itemsStr = items.stream()
                .sorted()
                .collect(Collectors.joining(ITEMS_DELIMITER));
        return String.format(CATEGORY_OUTPUT_FORMAT, name, itemsStr);
    }
}
