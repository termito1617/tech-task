package category;

import category.entities.AnimalsCategory;
import category.entities.CarsCategory;
import category.entities.Category;
import category.entities.NumbersCategory;
import category.loader.CategoriesLoader;
import category.loader.impl.CategoriesLoaderImpl;
import category.utils.ArgumentsParser;

import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.List;

/**
 * Demo class.
 **/
public class Application implements AutoCloseable {
    private static final String DEFAULT_FILE_NAME = "input.txt";
    private static final String FILE_PARAMETER = "file";

    private CategoriesLoader categoriesLoader;
    private PrintWriter writer;

    public Application(CategoriesLoader categoriesLoader, OutputStream outputStream) {
        this.categoriesLoader = categoriesLoader;
        this.writer = new PrintWriter(outputStream);
    }

    /**
     * Loads all categories via {@link CategoriesLoader} and prints all of them to provided output stream.
     **/
    public void run() {
        List<Category> categories = Arrays.asList(new AnimalsCategory(), new NumbersCategory(), new CarsCategory());

        categoriesLoader.loadCategoriesData(categories);

        categories.forEach(writer::println);
    }

    @Override
    public void close() throws Exception {
        categoriesLoader.close();
        writer.close();
    }

    public static void main(String[] args) throws Exception {
        String filePath = ArgumentsParser.parse(args).getOrDefault(FILE_PARAMETER, DEFAULT_FILE_NAME);

        CategoriesLoader categoriesLoader = new CategoriesLoaderImpl(new FileInputStream(new File(filePath)));
        try (Application application = new Application(categoriesLoader, System.out)) {
            application.run();
        }
    }
}
